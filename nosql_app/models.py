from datetime import datetime
from django.db import models

# Create your models here.
from mongoengine import StringField, IntField, DateTimeField, BooleanField, DynamicDocument, EmailField, ListField


class Post(DynamicDocument):
    user = EmailField(max_length=200)
    title = StringField(max_length=200, required=True)
    text = StringField(required=True)
    text_length = IntField()
    tags = ListField(StringField(max_length=16))
    date_modified = DateTimeField(default=datetime.utcnow())
    is_published = BooleanField()

    def save(self, *args, **kwargs):
        self.text_length = len(self.text)
        return super(Post, self).save(*args, **kwargs)


def get_post_model():
    name = 'Post'
    class Meta:
        app_label = 'nosql_app'

    attrs = dict()
    attrs['Meta'] = Meta
    attrs['user'] = EmailField(max_length=200)
    attrs['title'] = StringField(max_length=200, required=True)
    attrs['text'] = StringField(required=True)
    attrs['text_length'] = IntField()
    attrs['tags'] = ListField(StringField(max_length=16))
    attrs['date_modeified'] = DateTimeField(default=datetime.utcnow())
    attrs['is_published'] = BooleanField()

    def save(self, *args, **kwargs):
        self.text_length = len(self.text)
        return super(name, self).save(*args, **kwargs)

    attrs['save'] = save

    model = type(name, (DynamicDocument,), attrs)

    return model


def get_fruit_model():
    name = 'Fruits'
    class Meta:
        app_label = 'nosql_app'

    attrs = dict()
    attrs['Meta'] = Meta
    attrs['user'] = EmailField(max_length=200)
    attrs['name'] = StringField(max_length=32)
    attrs['color'] = StringField(max_length=16)
    attrs['shape'] = StringField(max_length=16)

    model = type(name, (DynamicDocument,), attrs)
    return model