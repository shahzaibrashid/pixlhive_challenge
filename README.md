# README #

Pixlhive Assessment project.

### Pre-Requisite ###

* Install required python packages from `requirements.txt` file.
* Restore Mysql dump file `mysql_dump.sql`.
* Restore MongoDb dump file from `mongodb_dump/dump`.
* Update project settings with proper mysql user/password.

### WebService ###

* For SQL App: http://localhost:8000/api/v1/
* For No-SQL App: http://localhost:8000/api/v2/