from django.conf.urls import patterns, include, url
from tastypie.api import Api
from nosql_app.api import PostResource, FruitResource
from sql_app.api import UserResource, ProfileResource
from nosql_app.views import *
from django.contrib import admin
admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(ProfileResource())
v2_api = Api(api_name='v2')
v2_api.register(PostResource())
v2_api.register(FruitResource())
# user_resource = UserResource()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pixlhive_challenge.views.home', name='home'),
    # url(r'^$', PostListView.as_view(), name='list'),
    # url(r'^blog/', include('nosql_app.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'^api/', include(v2_api.urls)),
)
