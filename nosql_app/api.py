import json
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import Authorization
from tastypie.constants import ALL
from tastypie_mongoengine.resources import MongoEngineResource
from nosql_app.models import Post, get_fruit_model
from sql_app.models import User

__author__ = 'srashid'


class PostResource(MongoEngineResource):
    class Meta:
        queryset = Post.objects.all()
        resource_name = 'post'
        # always_return_data = True
        filtering = {
            'user': ALL,
        }
        authorization = Authorization()
        authentication = BasicAuthentication()

    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.email
        return bundle


class FruitResource(MongoEngineResource):
    class Meta:
        queryset = get_fruit_model().objects.all()
        resource_name = 'fruit'
        authorization = Authorization()
        authentication = BasicAuthentication()

    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.email
        return bundle